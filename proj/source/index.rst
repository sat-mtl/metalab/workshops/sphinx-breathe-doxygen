.. HelloWorld documentation master file, created by
   sphinx-quickstart on Mon Jul 26 07:49:05 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HelloWorld's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Using `doxygenindex`: 

.. doxygenindex::

Using `doxygenstruct`:
 
.. doxygenstruct:: greetings


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
