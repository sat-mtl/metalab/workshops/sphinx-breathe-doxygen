# Sphinx - Doxygen - Breathe

*notes for a workshop*

resources: 
https://breathe.readthedocs.io/en/latest/quickstart.html

## installation

- install breathe in the same virtualenv as Sphinx
    
        pip install breathe
    
- install doxygen

## set up - Doxygen

- have a C++ example with doxygen comments
- generate a Doxyfile:

        doxygen -g
    
- make it generate XML by finding the `GENERATE_XML` line and setting it up to `YES`

- generate XML files:

        doxygen
    
## set up - Sphinx

- use Sphinx to generate a project:

        sphinx-quickstart
    
- answer the questions with placeholders if needed

- uncomment these three lines at the top of the `proj/source/conf.py` file:

        import os
        import sys
        sys.path.insert(0, os.path.abspath('.'))

- in the `conf.py` file, add these lines:

        extensions = ['breathe' ]
        breathe_projects = { "myproject": "../xml/" }
        breathe_default_project = "myproject"
    
## now ready to use Breathe in Sphinx !

The following rst commands are not available to you:

    .. doxygenindex::
    .. doxygenfunction::
    .. doxygenstruct::
    .. doxygenenum::
    .. doxygentypedef::
    .. doxygenclass::
    
Include some in the `index.rst` file to use them !

## make the docs

- use the Makefile to build the docs:

        make html

## Done !

If there is anything regarding these notes, please open an issue :bouquet:
    
